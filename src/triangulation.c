#include "triangluation.h"


void get_coords(global_xyz *xyz, double dist[4][4]){
    uint8_t size = xyz->size;

    if(size == 0) return;
    if(size > 0){
        /* set initiator/master to origin */
        xyz->n1.x = 0;
        xyz->n1.y = 0;
        xyz->n1.z = 0;
    }
    if(size > 1){
        /* set second node to x-axis of origin */
        xyz->n2.x = dist[0][1];
        xyz->n2.y = 0;
        xyz->n2.z = 0;
    }
    if(size > 2){
        /* set third node to same z-plane as first and second node */
        xyz->n3.x = (dist[1][2]*dist[1][2] - dist[0][2]*dist[0][2] - dist[0][1]*dist[0][1])/(-2*dist[0][1]*dist[0][1]);
        xyz->n3.y = sqrt(dist[0][2]*dist[0][2]- xyz->n3.x*xyz->n3.x);
        xyz->n3.z = 0;
    }
    if(size > 3){
        /* get fourth node coords */
        xyz->n4.x = (dist[1][3]*dist[1][3] - dist[0][3]*dist[0][3] - dist[0][1]*dist[0][1])/(-2*(dist[0][1])); 
        xyz->n4.y = (dist[2][3]*dist[2][3] - dist[0][3]*dist[0][3] 
                    - ((xyz->n4.x - xyz->n3.x)*(xyz->n4.x - xyz->n3.x) - xyz->n4.x*xyz->n4.x) 
                    - xyz->n3.y*xyz->n3.y)/(-2*xyz->n3.y);
        xyz->n4.z = sqrt(dist[0][3]*dist[0][3] - xyz->n4.x*xyz->n4.x - xyz->n4.y*xyz->n4.y);
    }
    return;    
}
