#include "triangulation.h"

// Initialize the global distance array which is used in get_coords()
double dist_arr[4][4]={
    {    0, 1.414, 2.828, 2.236},
    { 1.414,    0, 2.449, 2.236},
    { 2.828, 2.449,    0, 2.000},
    { 2.236, 2.236, 2.000,    0},
};

// Initialize the global struct that houses the coordinates of all nodes
global_xyz xyz = {4, {0.0,0.0,0.0,0.0}, {0.0,0.0,0.0,0.0}, {0.0,0.0,0.0,0.0}, {0.0,0.0,0.0,0.0}};

void app_main(){
    while(1){
        vTaskDelay(2000/ portTICK_RATE_MS);
        // Pass in global struct and dist array
        get_coords(&xyz, dist_arr);
        // Print the results which are housed in the global struct
        printf("---------- Post Calculation results ----------\n");
        printf("Node \t X \t\t Y \t\t Z\n");
        printf("1 \t %f \t %f \t %f\n", xyz.n1.x, xyz.n1.y, xyz.n1.z);
        printf("2 \t %f \t %f \t %f\n", xyz.n2.x, xyz.n2.y, xyz.n1.z);
        printf("3 \t %f \t %f \t %f\n", xyz.n3.x, xyz.n3.y, xyz.n1.z);
        printf("4 \t %f \t %f \t %f\n", xyz.n4.x, xyz.n4.y, xyz.n4.z);
        vTaskDelay(2000/ portTICK_RATE_MS);
    }
}
