/** @file */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "driver/spi_master.h"
#include "soc/gpio_struct.h"
#include "driver/gpio.h"

/**
 * Takes in a 2D array which contains all node distances and a struct which
contains all four node coordinates and the number of nodes in the mesh; calculates all four node coordinates and fills in the input struct.
 */
void get_coords(global_xyz *, double [4][4]);

/**
 * @struct coords
 * @brief The struct that houses the node’s x, y, &amp; z coordinates and the quality factor of
the coordinate (unused).
 * @var coords::x
 * Contains the x coordinate of the node
 * @var coords::y
 * Contains the y coordinate of the node
 * @var coords::z
 * Contains the z coordinate of the node
 * @var coords::qf
 * Contains the quality factor of the nodes coordinates
 */
typedef struct {
    double x; 
    double y;
    double z;
    uint8_t qf;
} coords;

/**
 * @struct global_xyz
 * @brief Contains the number of nodes that are present in the system and the coordinates
of each of the nodes.
 * @var global_xyz::size
 * Number of nodes present in the system
 * @var global_xyz::n1
 * Contains the coordinate information for node 1
 * @var global_xyz::n2
 * Contains the coordinate information for node 2
 * @var global_xyz::n3
 * Contains the coordinate information for node 3
 * @var global_xyz::n4
 * Contains the coordinate information for node 4
 */
typedef struct {
    uint8_t size;
    coords n1;
    coords n2;
    coords n3;
    coords n4;
}global_xyz;